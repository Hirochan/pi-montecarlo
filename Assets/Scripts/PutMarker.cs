﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutMarker : MonoBehaviour
{
    public GameObject Marker;
    public float x, y;
    public float r;
    public float CountTime;
    public float Span;

    public float U;
    public float I;
    public float PI;
    // Start is called before the first frame update
    void Start()
    {
        r = gameObject.transform.localScale.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (CountTime > Span)
        {
            SetMarker();
            CountTime = 0f;
        }
        CountTime += Time.deltaTime;
    }
    void SetMarker()
    {
        x = Random.Range(transform.position.x - r, transform.position.x + r);
        y = Random.Range(transform.position.y - r, transform.position.y + r);
        Vector2 pos = new Vector2(x, y);
        GameObject obj = Instantiate(Marker, pos, Quaternion.identity);
        if (InCircle())
        {
            obj.GetComponent<SpriteRenderer>().color = Color.green;
            I++;
        }
        U++;
        PI = I / U * 4;
    }
    bool InCircle()
    {
        float X = x - transform.position.x;
        float Y = y - transform.position.y;
        return X * X + Y * Y < r * r;
    }
}
