﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;

public class Cluculater : MonoBehaviour
{
    public GameObject[] Fields;
    public float Max,Min,Ave;
    public int digit = 13;
    public TextMeshProUGUI MaxT, MinT, AveT;
    // Start is called before the first frame update
    void Start()
    {
        MaxT.text = "Max : 0.0000000000000";
        MinT.text = "Min : 0.0000000000000";
        AveT.text = "Ave : 0.0000000000000";
    }

    // Update is called once per frame
    void Update()
    {
        //Max = Mathf.Max((float[])Fields.Select((GameObject C) => { return C.GetComponent<PutMarker>().PI; }));
        //Min = Mathf.Min((float[])Fields.Select((GameObject C) => { return C.GetComponent<PutMarker>().PI; }));
        //Ave = Fields.Select((GameObject C) => { return C.GetComponent<PutMarker>().PI; }).Sum();
        Max = Datas()[0];
        Min = Datas()[1];
        Ave = Datas()[2];
        MaxT.text = string.Format("Max : {0:F6}", Max);
        MinT.text = string.Format("Min : {0:F6}", Min);
        AveT.text = string.Format("Ave : {0:F6}", Ave);

    }
    public float[] Datas()
    {
        float max = 0f, min = 10f, sum=0f;
        for(int i = 0; i < Fields.Length; i++)
        {
            float x = Fields[i].GetComponent<PutMarker>().PI;
            if (x>max)
            {
                max = x;
            }
            if (x < min)
            {
                min = x;
            }
            sum += x;
        }
        return new float[3] { max, min, sum / Fields.Length };
    }
   
}
